package location;

import java.io.IOException;

import org.json.JSONException;

import de.taimos.gpsd4java.api.IObjectListener;
import de.taimos.gpsd4java.backend.AbstractResultParser;
import de.taimos.gpsd4java.backend.GPSdEndpoint;
import de.taimos.gpsd4java.backend.ResultParser;
import pw.guapiston.ShellManager;

public class LocationManager {
	
	private GPSdEndpoint gpsEndpoint;
	private int processId;
	ShellManager shellManager;

	public LocationManager(IObjectListener listener){
			shellManager = new ShellManager();	
			lanzarServicioGPS();
		try {
			gpsEndpoint= new GPSdEndpoint("localhost", 2947, new ResultParser());
			escucharCambiosDePosisicon(listener);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void escucharCambiosDePosisicon(IObjectListener listener){
		gpsEndpoint.addListener(listener);
		gpsEndpoint.start();
		try {
			gpsEndpoint.watch(true, true);
		} catch (IOException | JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void dejarDeEscucharCambiosDePosicion(IObjectListener listener){
		gpsEndpoint.removeListener(listener);
		gpsEndpoint.stop();
		
	}
	public void lanzarServicioGPS(){
		System.out.println("Lanzando GPS");
		try {
			processId = shellManager.executeCommand("gpsd -D 5 -N -n /dev/ttyAMA0");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void detenerServicioGPS(){
		if(processId>0)
			shellManager.killProcess(processId);
	}
}
