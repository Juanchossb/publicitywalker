package networking;

import java.util.List;

import model.Video;
import model.Walker;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

public interface NetworkApi {
	
	//@GET("videos")
	//Call<List<Video>> cargarVideos(@Query("latitud") String latitud, @Query("longitud") String longitud);
	@GET("y0151")
	Call<List<Video>> cargarVideos();
	@GET("14xhb5")
	Call<Walker> obtenerWalker(@Query("raspberryId")String raspberryId);
	@GET("y0151")
	Call<List<Video>> reportarReproduccion(@Query("idVideo") String idVideo,@Query("idWalker")String idWalker,@Query("latitud")String latitud,@Query("longitud")String longitud,@Query("millis")String millis);

}
