package networking;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.common.eventbus.EventBus;

import model.Video;
import model.Walker;
import pw.guapiston.ShellManager;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class IdentificacionController implements Callback<Walker>{
static final String BASE_URL="https://api.myjson.com/bins/";
	
	private ShellManager shellManager;
	private WalkerInformationListener listener;

	public void obtenerInformacionDelWalker(WalkerInformationListener listener){
		this.listener = listener;
      Retrofit retrofit = new Retrofit.Builder()
                      .baseUrl(BASE_URL)
                      .addConverterFactory(GsonConverterFactory.create())
                      .build();
      
      NetworkApi networkapi =retrofit.create(NetworkApi.class);
      
      Call<Walker> call =networkapi.obtenerWalker(obtenerCodigoUnicoRaspberry());
      call.enqueue(this);
	}
	private String obtenerCodigoUnicoRaspberry(){
		shellManager=new ShellManager();
		List<String> listaRespuesta = null;
		try {
			listaRespuesta=shellManager.executeCommand(new String[]{"/home/pi/walkerapi/raspberryPiID.sh"});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaRespuesta.get(0);
	}

	@Override
	public void onFailure(Throwable arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onResponse(Response<Walker> arg0, Retrofit arg1) {
		Walker walker = arg0.body();
		System.out.println("Walker "+walker.nombre.toUpperCase()+" logeado con éxito");
		listener.onWalkerInformationReceived(walker);
	}
	
	public interface WalkerInformationListener{
		void onWalkerInformationReceived(Walker walker);
	}
}
