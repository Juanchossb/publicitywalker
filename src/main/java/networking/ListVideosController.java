package networking;

import java.util.ArrayList;
import java.util.List;

import com.google.common.eventbus.EventBus;

import model.Posicion;
import model.Video;
import model.Walker;
import pw.guapiston.FileManager;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class ListVideosController implements Callback<List<Video>>{
	static final String BASE_URL="https://api.myjson.com/bins/";
	
	
	ListaVideosListener listener;
	List<Video> listaVideos;
	Retrofit retrofit;
	NetworkApi networkapi;
	private FileManager filemanager ;
	
	public ListVideosController(FileManager filemanager, ListaVideosListener listener){
		 Retrofit retrofit = new Retrofit.Builder()
                 .baseUrl(BASE_URL)
                 .addConverterFactory(GsonConverterFactory.create())
                 .build();
 
  networkapi =retrofit.create(NetworkApi.class);
  this.listener = listener;
  this.filemanager = filemanager;
	}

	public void mockVideosConLatitudYLongitud(String latitud, String longitud){
			listener.onListaVideosReceived(filemanager.getVideosInDirectory());
		
	}
	public void obtenerVideosConLatitudYLongitud(String latitud,String longitud){
		System.out.println("Obteniendo lista de reproduccion en las coordenadas: "+latitud+","+longitud);
		
			listaVideos=new ArrayList();
         
          Call<List<Video>> call =networkapi.cargarVideos();
          call.enqueue(this);
	}
	public void reportarVideoReproducido(long millis, Video video, Posicion posicion, Walker walker){
		if(posicion==null)
			posicion = new Posicion(0,0);
		Call<List<Video>> call=networkapi.reportarReproduccion(String.valueOf(video.id),walker.walkerId , String.valueOf(posicion.latitud), String.valueOf(posicion.longitud), String.valueOf(System.currentTimeMillis()));
		call.enqueue(this);
	}
	
	public void mockReportarVideoReproducido(long millis, Video video, Posicion posicion, Walker walker) {
		listener.onListaVideosReceived(filemanager.getVideosInDirectory());
		
	}

	public void onFailure(Throwable arg0) {
		// TODO Auto-generated method stub
		arg0.printStackTrace();
		
	}

	public void onResponse(Response<List<Video>> arg0, Retrofit arg1) {
		if(arg0.isSuccess()){
			listaVideos = arg0.body();
		
			listener.onListaVideosReceived(listaVideos);			
		}else{
			//No recibimos la lista
			System.out.println(arg0.errorBody());
		}
		
	}
	
	public interface ListaVideosListener{
		void onListaVideosReceived(List<Video> listaVideos);
	}

}
