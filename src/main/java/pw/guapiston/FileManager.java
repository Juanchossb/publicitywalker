package pw.guapiston;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

import model.Video;
import model.Walker;

import org.apache.commons.io.FileUtils;
import org.imgscalr.Scalr;

import com.google.common.eventbus.EventBus;

public class FileManager {
	EventBus eventBus;
	private static String DIRECTORY="/home/pi/walkerapi/videos/";
	private static int SCREEN_PIXELS=32;//Pixeles de la pantalla 32x32
	private List<Integer> delays;
	ShellManager shellManager;
	
	public void downloadFile(Video video) throws IOException{
		
		if(shellManager==null)
			shellManager=new ShellManager();
		
		URL src = new URL(video.url);
		File fakeFile = new File(DIRECTORY+"_"+video.nombre);
		File realFile = new File(DIRECTORY+video.nombre);
		System.out.println("Iniciando descarga de video: "+video.nombre);
		FileUtils.copyURLToFile(src, fakeFile);
		System.out.println("Video: "+video.nombre+" descargado con éxito, Cambiando tamaño a 32x32");
		String commandResizeGif ="./home/pi/walkerapi/resizer.sh "+fakeFile.getAbsolutePath()+" "+realFile.getAbsolutePath();
		try {
			video.downloadProcess=shellManager.executeCommandForProcess(commandResizeGif);
			video.file=realFile;
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
	}
	public void downloadFile(Walker walker) throws Exception{
		if(shellManager==null)
			shellManager=new ShellManager();

		URL src = new URL(walker.imagen);
		File fakeFile = new File(DIRECTORY+"_walker.jpg");
		File realFile = new File(DIRECTORY+"walker.jpg");
		System.out.println("Descargando archivo de Imagen de Walker" );
		FileUtils.copyURLToFile(src, fakeFile);
		System.out.println("Cambiando el tamano de la imagen de walker recibida.");
		String commandResizeGif ="./home/pi/walkerapi/resizer.sh "+fakeFile.getAbsolutePath()+" "+realFile.getAbsolutePath();
		try {
			walker.downloadProcess=shellManager.executeCommandForProcess(commandResizeGif);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		
	}
	
	public boolean isFileInDirectory(String name){
		File queryFile = new File(DIRECTORY+name);
		return queryFile.exists();
	}

	public List<Video> getVideosInDirectory(){
		File folder = new File(DIRECTORY);
		File[] fileList = folder.listFiles();
		List<Video> videoList = new ArrayList<>();
		for(File file:fileList) {
			videoList.add(new Video(file.getName()));
		}
		
		return videoList;
	}
	private List<BufferedImage> splitGifIntoImages(File imageGif) throws IOException{
		ImageReader reader = ImageIO.getImageReadersByFormatName("gif").next();
		ImageInputStream stream = ImageIO.createImageInputStream(imageGif);
	    reader.setInput(stream);
	   
	    List<BufferedImage> listaRespuesta=new ArrayList();
	    int count = reader.getNumImages(true);
	    
	    for (int index = 0; index < count; index++) {
	        BufferedImage frame = reader.read(index);
	        listaRespuesta.add(scaleImagetoFillScreen(frame));
	        IIOMetadataNode root = (IIOMetadataNode) reader.getImageMetadata(0).getAsTree("javax_imageio_gif_image_1.0");
	        IIOMetadataNode gce = (IIOMetadataNode) root.getElementsByTagName("GraphicControlExtension").item(0);
	        int delay = Integer.valueOf(gce.getAttribute("delayTime"));
	        delays.add(delay);
	    }
	    return listaRespuesta;
	}
	private BufferedImage scaleImageMainainingAspect(BufferedImage image){
		return Scalr.resize(image,SCREEN_PIXELS);
	}
	
	private BufferedImage scaleImagetoFillScreen(BufferedImage image){
		return Scalr.resize(image, Scalr.Method.SPEED, Scalr.Mode.FIT_EXACT,SCREEN_PIXELS,SCREEN_PIXELS,Scalr.OP_BRIGHTER);
	}
	private void saveImageListToFile(List<BufferedImage> list, File file) throws FileNotFoundException, IOException{
		ImageOutputStream output =
			       new FileImageOutputStream(file);

			     GifSequenceWriter writer =
			       new GifSequenceWriter( output, list.get(0).getType(), delays.get(0)*10, true );

			     writer.writeToSequence( list.get(0) );
			     for ( int i = 1; i < list.size(); i++ ) {
			    	 
			        BufferedImage nextImage = list.get(i);
			        writer.writeToSequence( nextImage );
			     }

			     writer.close();
			     output.close();
	}

}
