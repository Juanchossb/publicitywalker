package pw.guapiston;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import model.Walker;
import networking.IdentificacionController;

/**
 * Main class PW
 */

public class App{

    public static Walker walker;
    static IdentificacionController identificacionController;
    public static final String REAL_OR_MOCK = "mock";


    public static void main(String[] args) {
        if(REAL_OR_MOCK.equals("real")) {
        	identificacionController = new IdentificacionController();
        identificacionController.obtenerInformacionDelWalker(walkerInformationListener);
        }else
        	walkerInformationListener.onWalkerInformationReceived(new Walker("walker de prueba","idDePrueba"));
    }


    private static boolean netIsAvailable() {
        try {
            final URL url = new URL("http://www.google.com");
            final URLConnection conn = url.openConnection();
            conn.connect();
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    private static IdentificacionController.WalkerInformationListener walkerInformationListener = new IdentificacionController.WalkerInformationListener() {

		@Override
		public void onWalkerInformationReceived(Walker walker) {
			 App.this.walker = walker;
	         Reproductor reproductor = new Reproductor( walker);
	         reproductor.launch();
		}
    	
    };


}
