package pw.guapiston;

import java.io.IOException;

public class NotificationManager {

	ShellManager shellManager;
	Process process;
	public NotificationManager(){
		shellManager = new ShellManager();
	}
	public void lanzarNotificacionNormal(String texto){
		System.out.println(texto);
		String [] split = texto.split(" ");		
		String [] commands = new String[split.length+1];
		commands[0]="sudo ~/rpi-rgb-led-matrix/examples-api-use/text-example -f ~/rpi-rgb-led-matrix/fonts/4x6.bdf -C 0,255,0 --led-no-hardware-pulse";
		for(int i=0;i<split.length;i++)
			commands[i+1]=split[i];
		
	try {
		process=shellManager.executeCommandForProcess(commands);
		process.isAlive();
	}catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
	}
	
}
