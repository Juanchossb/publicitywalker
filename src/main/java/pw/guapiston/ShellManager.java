package pw.guapiston;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ShellManager {
	Process process;

	public Process getProcess(){
		return process;
	}
	public List<String> executeCommand(String command[]) throws IOException{
		
		List<String> listaRespuesta=new ArrayList();
		process = Runtime.getRuntime().exec(command);                                                                                                                                                     
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String s;
		 while ((s = stdInput.readLine()) != null){
			 listaRespuesta.add(s);
			 System.out.println(s);
		 }
		 
		 return listaRespuesta;
	}
	
	public int executeCommand(String command) throws Exception{
		process = Runtime.getRuntime().exec(command);                                                                                                                                                     
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
	
			java.lang.reflect.Field f = process.getClass().getDeclaredField("pid");
		    f.setAccessible(true);
		    int pid = (Integer) f.get(process);
		return  pid;
		
	}
	
	public void killProcess(int processId){
		try {
			System.out.println("Destruyendo todos los procesos");
			executeCommand("sudo kill "+processId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void killAllProcesses(){
		try {
			process = Runtime.getRuntime().exec("sudo killall led-image-viewer");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Process executeCommandForProcess(String command) throws Exception{
		process = Runtime.getRuntime().exec(command);                                                                                                                                                     
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
	return process;
	}
	public Process executeCommandForProcess(String command[]) throws Exception{
		process = Runtime.getRuntime().exec(command);                                                                                                                                                     
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
	return process;
	}
}
