package pw.guapiston;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import de.taimos.gpsd4java.api.IObjectListener;
import de.taimos.gpsd4java.types.ATTObject;
import de.taimos.gpsd4java.types.DeviceObject;
import de.taimos.gpsd4java.types.DevicesObject;
import de.taimos.gpsd4java.types.SKYObject;
import de.taimos.gpsd4java.types.TPVObject;
import de.taimos.gpsd4java.types.subframes.SUBFRAMEObject;

import location.LocationManager;
import model.Posicion;
import model.Video;
import model.Walker;
import networking.ListVideosController;
import networking.ListVideosController.ListaVideosListener;

public class Reproductor {

    
    private static List<Video> listaVideos;
    private static List<Video> listaVideosSinOrganizar = new ArrayList();
    static final String TEMP_LATITUD = "4.6658462";
    static final String TEMP_LONGITUD = "-74.1015281";
    static FileManager fileManager;
    private static Video videoReproduccionActual;
    private static ShellManager shellManager;
    private static LocationManager locationManager;
    public static Posicion currentPosicion;
    public static ListVideosController videoController;
    private static Walker walker;


    public Reproductor(Walker walker) {
        this.walker = walker;
        fileManager = new FileManager();
        shellManager = new ShellManager();
        videoController = new ListVideosController(fileManager,listaVideoListener);
        if(App.REAL_OR_MOCK.equals("real"))
        locationManager = new LocationManager(LocationListener);
    }

    public void launch() {

        if (!isListaVideosEmpty()) {
            listaVideos = organizarListaPorFrecuencia(listaVideos);
        } else {
               
            if(App.REAL_OR_MOCK.equals("real"))
            videoController.obtenerVideosConLatitudYLongitud(TEMP_LATITUD, TEMP_LONGITUD);
            else
            	videoController.mockVideosConLatitudYLongitud(TEMP_LATITUD, TEMP_LONGITUD);
            
        }
    }

    private void mostrarImagenWalker() {
        if (!walker.isFileDownloaded(fileManager)) {
            walker.downloadFile(fileManager);
            while (walker.downloadProcess.isAlive()) {

            }
            System.out.println("Imagen de walker lista para ser mostrada");
            walker.mostrarImagen(shellManager);

        } else {
            walker.mostrarImagen(shellManager);
        }
    }

    public static Video playNext() {
        Video tempvid = null;
        if (videoReproduccionActual == null) {
            tempvid = listaVideos.get(0);

        } else {
            for (int i = 0; i < listaVideos.size(); i++)
                if (listaVideos.get(i).idReproduccion == videoReproduccionActual.idReproduccion)
                    if (i != listaVideos.size() - 1)
                        tempvid = listaVideos.get(i + 1);
                    else
                        tempvid = listaVideos.get(0);

        }
        videoReproduccionActual = tempvid;
        if (tempvid.downloadProcess == null)
            try {
                tempvid.downloadProcess = shellManager.executeCommandForProcess("echo Video descargado anteriormente, ignorando... " + tempvid.nombre);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        if (!tempvid.isStillDownloading())
            try {
                tempvid.reproducirVideo(shellManager);
                checkVideoPlaying(tempvid).start();;

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        else {
            try {
                Thread.sleep(250);

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            playNext();
        }

        return tempvid;
    }

    private static Thread checkVideoPlaying(Video video) {
    
    	return new Thread(new Runnable() {

			@Override
			public void run() {
				while(video.isPlaying())
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
				playNext();
				
			}
    		
    	});
    }
    private static Thread cronometrarVideo() {
        Thread thread = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(videoReproduccionActual.duracion * 1000);
                    videoReproduccionActual.detenerReproduccion(shellManager);
                    if(App.REAL_OR_MOCK.equals("real"))
                    videoController.reportarVideoReproducido(System.currentTimeMillis(), videoReproduccionActual, currentPosicion, walker);
                    else
                    	videoController.mockReportarVideoReproducido(System.currentTimeMillis(), videoReproduccionActual, currentPosicion, walker);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Thread.currentThread().interrupt();
                playNext();
            }
        });
        return thread;
    }

    private void reset() {
        launch();
    }


    private boolean isListaVideosEmpty() {
        boolean respuesta = true;
        if (listaVideos != null)
            if (listaVideos.size() > 0)
                respuesta = true;
            else
                respuesta = false;
        else {
            listaVideos = new ArrayList();
            respuesta = true;
        }
        return respuesta;
    }

    private static List<Video> organizarListaPorPrioridad(List<Video> lista) {
        int pointer = 0;
        while (pointer < lista.size() - 1)
            for (int i = 1; i < lista.size(); i++) {
                int comparison = checkVideosAgainsEachOtherByPriority(lista.get(pointer), lista.get(i));
                if (comparison == 0)
                    pointer = i;
                else {
                    //Si la priodada es mas baja lo mandamos al primer lugar y empezamos denuevo
                    switchPlacesInList(lista, 0, i);

                    pointer = 0;
                }


            }
        return lista;
    }

    private static int checkVideosAgainsEachOtherByPriority(Video firstVideo, Video secondVideo) {
        if (firstVideo.prioridad <= secondVideo.prioridad)
            return 0;
        else
            return 1;
    }

    private static List<Video> organizarListaPorFrecuencia(List<Video> lista) {
        List<Map<Integer, Video>> listsd = new ArrayList();
        for (int y = 0; y < lista.size(); y++) {
            Video video = checkVideoDownload(lista.get(y));

            if (video.frecuencia > 1) {
                int temp = (lista.size() + video.frecuencia) / video.frecuencia;
                Map<Integer, Video> mapa = new HashMap();
                for (int x = 1; x <= video.frecuencia; x++) {
                    int indexAPoner = temp * x;
                    mapa.put(indexAPoner, video);
                }
                listsd.add(mapa);
            }
        }

        List<Video> tempList = new ArrayList();
        for (int y = 0; y < lista.size(); y++) {
            tempList.add(addRandomIdReproduccion(lista.get(y)));

            for (Map<Integer, Video> listac : listsd) {
                Video tmpvid = listac.get(y);
                if (tmpvid != null) {
                    Video cloneVideo = new Video(tmpvid);
                    tempList.add(addRandomIdReproduccion(cloneVideo));
                }
            }
        }

        lista = tempList;
        return lista;
    }

    private static Video addRandomIdReproduccion(Video video) {
        if (video.idReproduccion == 0)
            video.idReproduccion = ThreadLocalRandom.current().nextInt(1000, 10000 + 1);
        return video;
    }

    private static Video checkVideoDownload(final Video video) {
        Video respuesta = video;
        if (!fileManager.isFileInDirectory(respuesta.nombre))
            new Thread(new Runnable() {

                public void run() {
                    try {
                        fileManager.downloadFile(video);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

            }).start();
        else if (video.file == null)
            video.file = new File("videos/" + video.nombre);
        return respuesta;
    }

    private static void switchPlacesInList(List<Video> list, int firstPlace, int secondPlace) {
        Video temp = list.get(firstPlace);
        list.set(firstPlace, list.get(secondPlace));

        if (firstPlace < secondPlace)
            for (int i = firstPlace + 1; i < secondPlace; i++) {
                Video temp2 = list.get(i);
                list.set(i, temp);
                temp = temp2;
            }
        else if (secondPlace < firstPlace)
            for (int i = secondPlace; i >= firstPlace; i--) {
                Video temp2 = list.get(i);
                list.set(i, temp);
                temp = temp2;
            }
    }


    private ListaVideosListener listaVideoListener= new ListaVideosListener() {

		@Override
		public void onListaVideosReceived(List<Video> listaVideos) {
		      boolean mismaLista = false;
	            for (int i = 0; i < listaVideosSinOrganizar.size(); i++) {
	                boolean tempMismaLista[] = new boolean[listaVideos.size()];

	                for (int y = 0; y < listaVideos.size(); y++) {
	                    if (listaVideosSinOrganizar.get(i).id == listaVideos.get(y).id) {
	                        tempMismaLista[y] = true;
	                    } else {
	                        tempMismaLista[y] = false;
	                    }
	                }
	                for (boolean bol : tempMismaLista)
	                    if (bol)
	                        mismaLista = true;
	            }
	            if (!mismaLista) {

	                System.out.println("Lista de videos obtenida. " + listaVideos.size() + " videos");
	                listaVideosSinOrganizar = listaVideos;
	                Reproductor.this.listaVideos = organizarListaPorFrecuencia(listaVideos);
	                System.out.println("lista obtenida y organizada");
	                playNext();
	            }			
		}
    	
    };



    private IObjectListener LocationListener = new IObjectListener() {

        public void handleATT(ATTObject arg0) {
            arg0.toString();
        }

        public void handleDevice(DeviceObject arg0) {
            arg0.toString();
        }

        public void handleDevices(DevicesObject arg0) {
            arg0.toString();

        }

        public void handleSKY(SKYObject arg0) {
            arg0.toString();
        }

        public void handleSUBFRAME(SUBFRAMEObject arg0) {
            arg0.toString();
        }

        public void handleTPV(TPVObject arg0) {
            currentPosicion = new Posicion(arg0);

        }

    };


}
