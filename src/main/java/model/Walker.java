package model;

import com.google.gson.annotations.SerializedName;

import pw.guapiston.FileManager;
import pw.guapiston.ShellManager;

public class Walker {
	@SerializedName("ip")
	public String ipAdress;
	@SerializedName("nombre")
	public String nombre;
	@SerializedName("imagen")
	public String imagen;
	@SerializedName("walkerId")
	public String walkerId;
	
	public Process downloadProcess;
	private int processID;
	
	public Walker(String nombre, String walkerID) {
		this.nombre = nombre;
		this.walkerId = walkerID;
	}
	
	public void mostrarImagen(ShellManager manager){
		manager.killAllProcesses();
		String comandoReproduccion= "sudo /home/pi/rpi-rgb-led-matrix/utils/led-image-viewer -f videos/walker.jpg"; 
		try {
			System.out.println("mostrando Imagen del walker");
			processID=manager.executeCommand(comandoReproduccion);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void deternerReproduccion(ShellManager manager){
		manager.killProcess(processID);
	}
	public boolean isFileDownloaded(FileManager manager){
		return manager.isFileInDirectory("walker.jpg");
	}
	public void downloadFile(FileManager manager){
		try {
			manager.downloadFile(this);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
