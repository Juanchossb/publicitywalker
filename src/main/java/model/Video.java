package model;

import java.io.File;
import java.io.IOException;
import java.text.Format.Field;

import org.eclipse.core.runtime.Platform;

import pw.guapiston.ShellManager;

import com.google.gson.annotations.SerializedName;

public class Video {
	@SerializedName("id")
	public int id;
	@SerializedName("prioridad")
	public int prioridad;
	@SerializedName("frecuencia")
	public int frecuencia;
	@SerializedName("url")
	public String url;
	@SerializedName("nombre")
	public String nombre;
	@SerializedName("idZona")
	public String idZona;
	@SerializedName("duration")
	public int duracion;
	public int idReproduccion=0;
	private int processID;
	public Process downloadProcess;
	public Process playProcess;
	public File file;
	
	public Video(Video video){
		this.id=video.id;
		this.prioridad=video.prioridad;
		this.frecuencia=video.frecuencia;
		this.url=video.url;
		this.nombre=video.nombre;
		this.idZona=video.idZona;
		this.duracion=video.duracion;
	}

	public Video(String nombre){
		this.nombre = nombre;
		this.duracion = 20;
	}
	public void reproducirVideo(ShellManager manager) throws IOException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
	//	manager.killAllProcesses();
		System.out.println("Iniciando reproduccion de: "+nombre+"   tiempo: "+duracion+" segundos");
		String comandoReproduccion= "sudo /home/pi/rpi-rgb-led-matrix/utils/video-viewer --led-no-hardware-pulse -L /home/pi/walkerapi/videos/"+nombre; 
		try {
			//processID=manager.executeCommand(comandoReproduccion);
			playProcess = manager.executeCommandForProcess(comandoReproduccion);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}	
	public String getComandoReproduccion(){
		return "sudo timeout "+duracion+" ./home/pi/rpi-rgb-led-matrix/utils/led-image-viewer /home/pi/walkerapi/videos/"+nombre; 
	}
	
	public void detenerReproduccion(ShellManager manager) throws IOException{
		
	 
		manager.killProcess(processID);
		System.out.println("deteniendo proceso");
	
		
	}
	public boolean isStillDownloading(){
		
		try{
		downloadProcess.exitValue();
	//	if(file.exists())
		return false;
		//else
			//return true;
		}catch(Exception e){
			return true;
		}
	}
	
	public boolean isPlaying() {
		try {
			playProcess.exitValue();
			return false;
		}catch(Exception e) {
			return true;
		}
	}
}
