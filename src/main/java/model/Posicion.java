package model;

import de.taimos.gpsd4java.types.TPVObject;

public class Posicion {
	public double latitud,longitud,velocidad,altura,millis;
	
	public Posicion(TPVObject object){
		this.latitud=object.getLatitude();
		this.longitud=object.getLongitude();
		this.velocidad=object.getSpeed();
		this.altura=object.getAltitude();
		this.millis=object.getTimestamp();
	}
	public Posicion(double latitud,double longitud){
		this.latitud=latitud;
		this.longitud=longitud;
	}

}
